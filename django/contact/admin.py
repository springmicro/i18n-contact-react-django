from django.contrib import admin
from contact.models import Address, Phone

# Register your models here.
admin.site.register(Address)
admin.site.register(Phone)
