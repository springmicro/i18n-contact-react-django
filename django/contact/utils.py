from django.conf import settings
import json
import os

contact_data = {}
with open(os.path.join(settings.BASE_DIR, 'contact', 'static', 'contactData.json'), 'r', encoding="utf-8") as f:
    contact_data = json.loads("".join(f.readlines()))
