from django.test import TestCase
from django.contrib.auth.models import User
from contact.models import Address, Phone

# More addresses to test: https://github.com/OpenCageData/address-formatting/tree/master/testcases/countries


class AddressTestCase(TestCase):
    EXAMPLE_EMAIL = "address@example.com"

    def setUp(self):
        User.objects.get_or_create(
            email=self.EXAMPLE_EMAIL, password="password")

    def test_po_box_not_physical(self):
        """PO Box addresses are not physical"""
        po_box = Address.objects.create(street_address="PO Box 12345", country="US", locality="Salt Lake City",
                                        region="UT", postal_code="84044", user=User.objects.get(email=self.EXAMPLE_EMAIL))
        self.assertEquals(po_box.is_physical, False)

    def test_po_box_format(self):
        """'PO Box' is formatted correctly on post_save"""
        PO_BOX_CORRECT = "PO Box"
        po_box = Address.objects.create(street_address="PO Box 12345", country="US", locality="Salt Lake City",
                                        region="UT", postal_code="84044", user=User.objects.get(email=self.EXAMPLE_EMAIL))

        variants = ["PO Box", "po box", "p.o. box",
                    "PO box", "P.O. box", "P.O.Box"]
        for variant in variants:
            po_box.street_address = variant + " 12345"
            po_box.save()
            self.assertIn(PO_BOX_CORRECT, po_box.street_address)


class PhoneTestCase(TestCase):
    EXAMPLE_EMAIL = "phone@example.com"

    def setUp(self):
        User.objects.get_or_create(
            email=self.EXAMPLE_EMAIL, password="password")

    def test_primary_phone(self):
        user = User.objects.get(email=self.EXAMPLE_EMAIL)
        """There can only be one primary phone per user."""
        Phone.objects.create(number="+15555550000", is_primary=True, user=user)
        Phone.objects.create(number="+15555550001", is_primary=True, user=user)
        Phone.objects.create(number="+15555550002", is_primary=True, user=user)
        self.assertEquals(
            len(Phone.objects.filter(user=user, is_primary=True)), 1)
        self.assertEquals(Phone.objects.get(
            user=user, is_primary=True).number, "+15555550002")
