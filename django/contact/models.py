from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from phonenumber_field.modelfields import PhoneNumberField
from addressformatting import AddressFormatter
from contact.utils import contact_data
import re

# https://schema.org/PostalAddress
# https://kitefaster.com/2017/05/03/maximum-string-length-popular-database-fields/


class Contact(models.Model):
    is_active = models.BooleanField(default=True)
    last_update = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(to=User, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Phone(Contact):
    number = PhoneNumberField(unique=True)

    allow_mfa = models.BooleanField(default=False)
    allow_sms = models.BooleanField(default=False)
    is_primary = models.BooleanField(default=False)

    def __str__(self):
        return self.number.__str__()

    @classmethod
    def post_create(cls, sender, instance, created, *args, **kwargs):
        """If this Phone is designated as the primary, make the user's other phones not their primary."""
        if instance.is_primary:
            Phone.objects.exclude(id=instance.id).filter(
                user=instance.user).update(is_primary=False)


post_save.connect(Phone.post_create, sender=Phone)


class Address(Contact):
    class Meta:
        verbose_name_plural = "Addresses"

    country = models.CharField(
        "Country", max_length=2, choices=((k, v) for k, v in contact_data["countries"].items()))
    locality = models.CharField("City or Locality", max_length=190)
    region = models.CharField(
        "Region, State, or Province", max_length=190, blank=True, null=True)
    postal_code = models.CharField("Postal Code", max_length=20)
    street_address = models.TextField("Street Address/PO Box")

    organization = models.CharField(max_length=200, blank=True, null=True)

    is_billing_address = models.BooleanField(default=True)
    is_mailing_address = models.BooleanField(default=True)
    is_physical_address = models.BooleanField(default=True)
    is_shipping_address = models.BooleanField(default=True)

    def user_name_order(self):
        # https://en.wikipedia.org/wiki/Personal_name#Name_order
        EASTERN_NAME_ORDER = ("CN", "HK", "HU", "JP",
                              "KH", "KP", "KR", "MN", "VN")
        if self.country in EASTERN_NAME_ORDER:
            return f"{self.user.last_name} {self.user.first_name}"
        else:
            return f"{self.user.first_name} {self.user.last_name}"

    def __str__(self):
        lines = ""
        if self.organization:
            lines += self.organization + "\n"
        if self.user.first_name and self.user.last_name:
            if self.organization:
                lines += "ATTN: "
            lines += self.user_name_order() + "\n"
        lines += self.street_address
        address = {
            # The AddressFormatter breaks down street addresses in many ways, but the "attention" is always at the top.
            "attention": lines,
            "city": self.locality,
            "country": contact_data["countries"][self.country]
        }
        if self.region:
            address["state"] = self.region
        if self.postal_code != "00000":
            address["postcode"] = self.postal_code
        return "\n".join([s for s in AddressFormatter().format(address, country=self.country).split("\n") if s and not s.isspace()])

    @classmethod
    def post_create(cls, sender, instance, created, *args, **kwargs):
        """PO Box addresses are not physical."""
        instance.street_address = re.sub(
            r"p\.*o\.* *box", "PO Box", instance.street_address, flags=re.IGNORECASE)
        if "po box" in instance.street_address.lower():
            instance.is_physical = False


post_save.connect(Address.post_create, sender=Address)
