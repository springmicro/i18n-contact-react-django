from asgiref.sync import sync_to_async
from django.http import response
from django.http.response import HttpResponse
from django.db.utils import IntegrityError
from rest_framework.authtoken.models import Token
from contact.api.serializers import AddressSerializer, PhoneSerializer
import httpx
import json
import os


def get_user_id(token):
    return Token.objects.get(key=token).user.id


async def post_address(data, headers):
    async with httpx.AsyncClient() as client:
        response = await client.post("http://127.0.0.1:8000/api/address/",
                                     data=data, headers=headers)
        try:
            response.raise_for_status()
        except httpx.HTTPStatusError as exc:
            response = exc.response
    # {json(), status_code}
    return response


async def post_phone(data, headers):
    response = None
    async with httpx.AsyncClient() as client:
        response = await client.post("http://127.0.0.1:8000/api/phone/",
                                     data=data, headers=headers)

        try:
            response.raise_for_status()
        except httpx.HTTPStatusError as exc:
            response = exc.response
    # {json(), status_code}
    return response


async def create_contact(request):
    # Get user info or return error
    user = None
    try:
        if "Authorization" in request.headers and "Token" in request.headers["Authorization"].split(" "):
            token = request.headers["Authorization"].split(" ")[1]
            user = await sync_to_async(get_user_id, thread_sensitive=True)(token)
    except Exception:
        return HttpResponse("User Not Found", 404)
    if user == None:
        return HttpResponse("Permission Denied", 403)

    request_data = dict(request.POST)
    phone_data = {"telephone": request_data.pop(
        "telephone")[0]} if "telephone" in request_data.keys() else {}
    request_data["user"] = user
    phone_data["user"] = user
    headers = {
        "Authorization": f"Api-Key {os.environ['REST_API_KEY']}"
    }
    response_data = {}
    errors = {}

    address_fields = set(dict(AddressSerializer().fields).keys())
    if set(request_data).issuperset(address_fields):
        try:
            r_address = await post_address(request_data, headers)
            if r_address.status_code >= 400 and r_address.status_code != 500:
                errors.update(r_address.json())
            elif r_address.status_code == 500:
                raise Exception(
                    "Something bad happened. Try submitting again or contact support.")
            else:
                response_data.update(r_address.json())
        except Exception as e:
            errors.update({"addressCountry": [str(e)]})

    phone_fields = set(dict(PhoneSerializer().fields).keys())
    if "telephone" in phone_data.keys() and set(phone_data).issubset(phone_fields):
        try:
            r_phone = await post_phone(phone_data, headers)
            if r_phone.status_code >= 400 and r_phone.status_code != 500:
                errors.update(r_phone.json())
            elif r_phone.status_code == 500:
                raise IntegrityError(
                    "Phone number already exists in database.")
            else:
                response_data.update(r_phone.json())
        except IntegrityError as e:
            errors.update({"telephone": [str(e)]})

    return HttpResponse(json.dumps({
        "data": response_data,
        "errors": errors
    }))
