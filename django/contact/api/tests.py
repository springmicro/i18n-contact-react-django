from django.core.exceptions import ValidationError
from django.test import TestCase
from contact.api.validators import postal_code_validator
import random
import string


class PostalCodeValidatorTestCase(TestCase):
    """
    Only a few tests are to ensure that the postal_code_validator function works.
    The function uses django-localflavor's PostalCodeFields for which there are already many tests defined.
        (see https://github.com/django/django-localflavor/blob/master/tests/)
    """

    def test_existing_validation_field_codes(self):
        """
        Run known tests to see if validator function matches Field
        Test case from https://github.com/django/django-localflavor/blob/master/tests/test_au/tests.py
        """
        error_format = ['Enter a 4 digit postcode.']
        valid = {
            '1234': '1234',
            '2000': '2000',
        }
        invalid = {
            'abcd': error_format,
            '20001': ['Ensure this value has at most 4 characters (it has 5).'] + error_format,
        }
        for key, value in valid.items():
            self.assertEquals(value, postal_code_validator("AR", key))
        for key, value in invalid.items():
            self.assertRaises(
                ValidationError, postal_code_validator, "AR", key)

    def test_is_postal_codes(self):
        """Check custom IS postal code validation."""
        self.assertEquals("101", postal_code_validator("IS", "101"))
        self.assertEquals("102", postal_code_validator("IS", "102"))
        self.assertRaises(ValidationError, postal_code_validator, "IS", "100")
        self.assertRaises(ValidationError, postal_code_validator, "IS", "abc")

    def test_generic_postal_codes(self):
        """Check postal code length."""
        def string_of_length(N):
            if N <= 0:
                return ""
            rand = ' '
            while rand[0] == ' ' or rand[0] == '-' or rand[-1] == ' ' or rand[-1] == '-':
                rand = ''.join(random.choices(string.ascii_uppercase +
                                              string.digits + " " + "-", k=N))
            return rand
        length0 = string_of_length(0)
        length2 = string_of_length(2)
        length11 = string_of_length(11)
        length12 = string_of_length(12)
        length21 = string_of_length(21)
        self.assertRaises(
            ValidationError, postal_code_validator, "XX", length0)
        self.assertEquals(length2, postal_code_validator("XX", length2))
        self.assertEquals(length11, postal_code_validator("XX", length11))
        self.assertEquals(length12, postal_code_validator("XX", length12))
        self.assertRaises(
            ValidationError, postal_code_validator, "XX", length21)
        self.assertRaises(
            ValidationError, postal_code_validator, "XX", "---")
        self.assertRaises(
            ValidationError, postal_code_validator, "XX", "   ")
        self.assertRaises(
            ValidationError, postal_code_validator, "XX", "*)@!$&!")
