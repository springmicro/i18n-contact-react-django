from django.conf.global_settings import DEBUG
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions, status
from rest_framework_api_key.permissions import HasAPIKey
from rest_framework.decorators import action
from django.core.exceptions import ValidationError
from contact.api.serializers import AddressSerializer, PhoneSerializer
from contact.models import Address, Phone
from contact.api.validators import postal_code_validator

api_permissions = [HasAPIKey]
if DEBUG:
    api_permissions = (HasAPIKey | permissions.IsAuthenticated)


class AddressViewSet(ModelViewSet):
    serializer_class = AddressSerializer
    permission_classes = api_permissions
    queryset = Address.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        obj = serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response({"address": obj.__str__()}, status=status.HTTP_201_CREATED, headers=headers)

    @action(detail=False, methods=['post'], permission_classes=[permissions.AllowAny])
    def validate_postal_code(self, request):
        try:
            if "addressCountry" in request.data and "postalCode" in request.data:
                postal_code_validator(
                    request.data["addressCountry"], request.data["postalCode"])
                return Response(["Valid!"], status=status.HTTP_200_OK)
            else:
                raise ValidationError(
                    "Please include addressCountry and postalCode in the request body.")
        except ValidationError as e:
            return Response(e, status=status.HTTP_400_BAD_REQUEST)


class PhoneViewSet(ModelViewSet):
    serializer_class = PhoneSerializer
    permission_classes = api_permissions
    queryset = Phone.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        obj = serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response({"phone": obj.__str__()}, status=status.HTTP_201_CREATED, headers=headers)

    @action(detail=False, methods=['post'], permission_classes=[permissions.AllowAny])
    def validate_phone_unique(self, request):
        try:
            if "telephone" in request.data:
                if len(self.queryset.filter(number=request.data["telephone"])) > 0:
                    raise ValidationError(
                        "Phone number already exists in database.")
                return Response(["Valid!"], status=status.HTTP_200_OK)
            else:
                raise ValidationError(
                    "Please include telephone in the request body.")
        except ValidationError as e:
            return Response(e, status=status.HTTP_400_BAD_REQUEST)
