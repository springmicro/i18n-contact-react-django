from contact.api.validators import postal_code_validator
from rest_framework import serializers
from django.core.exceptions import ValidationError
from contact.models import Address, Phone
from contact.utils import contact_data
from phonenumber_field.serializerfields import PhoneNumberField
from django.contrib.auth.models import User


class AddressSerializer(serializers.ModelSerializer):

    # https://schema.org/PostalAddress
    addressCountry = serializers.CharField(source='country')
    addressLocality = serializers.CharField(source='locality')
    addressRegion = serializers.CharField(source='region')
    streetAddress = serializers.CharField(source='street_address')
    postalCode = serializers.CharField(source='postal_code')

    organization = serializers.CharField(required=False)
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    class Meta:
        model = Address
        fields = ('addressCountry', 'addressLocality',
                  'addressRegion', 'streetAddress', 'postalCode', 'organization', 'user')

    def validate(self, data):
        errors = []

        data["locality"] = data["locality"].title()
        if not data['country'] in contact_data['countries']:
            errors.append(("addressCountry", "Invalid ISO-2 Country Code"))
        valid_regions = [p['value'] for p in contact_data['provinces']
                         if p['country'] == data['country']]
        if len(valid_regions) > 0 and data['region'] and not data['region'] in valid_regions:
            errors.append(("addressRegion", "Invalid Region Name"))
        try:
            postal_code_validator(
                data['country'], data['postal_code'])
        except ValidationError as e:
            errors.append(("postalCode", e))

        if len(errors) > 0:
            raise ValidationError(dict(errors))

        return data

    def create(self, validated_data):
        queryset = Address.objects.filter(**validated_data)
        if len(queryset) == 0:
            obj = Address.objects.create(**validated_data)
            obj.save()
            return obj
        else:
            return queryset.first()


class PhoneSerializer(serializers.ModelSerializer):
    telephone = PhoneNumberField(source='number')
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    is_primary = serializers.BooleanField(default=False, required=False)

    class Meta:
        model = Phone
        fields = ('telephone', 'user', 'is_primary')

    def create(self, validated_data):
        if len(Phone.objects.filter(user=validated_data["user"], is_primary=True)) == 0:
            validated_data['is_primary'] = True
        obj = Phone.objects.create(**validated_data)
        obj.save()
        return obj
