from django.core.exceptions import ValidationError
from django.forms.fields import CharField
import re

from localflavor.ar.forms import ARPostalCodeField
from localflavor.at.forms import ATZipCodeField
from localflavor.au.forms import AUPostCodeField
from localflavor.be.forms import BEPostalCodeField
from localflavor.br.forms import BRZipCodeField
from localflavor.ca.forms import CAPostalCodeField
from localflavor.ch.forms import CHZipCodeField
from localflavor.cn.forms import CNPostCodeField
from localflavor.cu.forms import CUPostalCodeField
from localflavor.cz.forms import CZPostalCodeField
from localflavor.de.forms import DEZipCodeField
from localflavor.dk.forms import DKPostalCodeField
from localflavor.ee.forms import EEZipCodeField
from localflavor.es.forms import ESPostalCodeField
from localflavor.fi.forms import FIZipCodeField
from localflavor.fr.forms import FRZipCodeField
from localflavor.gb.forms import GBPostcodeField
from localflavor.gr.forms import GRPostalCodeField
from localflavor.hr.forms import HRPostalCodeField
from localflavor.id_.forms import IDPostCodeField
from localflavor.il.forms import ILPostalCodeField
from localflavor.in_.forms import INZipCodeField
from localflavor.ir.forms import IRPostalCodeField
from localflavor.is_.is_postalcodes import IS_POSTALCODES
from localflavor.it.forms import ITZipCodeField
from localflavor.jp.forms import JPPostalCodeField
from localflavor.lt.forms import LTPostalCodeField
from localflavor.lv.forms import LVPostalCodeField
from localflavor.ma.forms import MAPostalCodeField
from localflavor.mt.forms import MTPostalCodeField
from localflavor.mx.forms import MXZipCodeField
from localflavor.nl.forms import NLZipCodeField
from localflavor.no.forms import NOZipCodeField
from localflavor.nz.forms import NZPostCodeField
from localflavor.pk.forms import PKPostCodeField
from localflavor.pl.forms import PLPostalCodeField
from localflavor.pt.forms import PTZipCodeField
from localflavor.ro.forms import ROPostalCodeField
from localflavor.ru.forms import RUPostalCodeField
from localflavor.se.forms import SEPostalCodeField
from localflavor.sg.forms import SGPostCodeField
from localflavor.si.forms import SIPostalCodeField
from localflavor.sk.forms import SKPostalCodeField
from localflavor.tr.forms import TRPostalCodeField
from localflavor.ua.forms import UAPostalCodeField
from localflavor.us.forms import USZipCodeField
from localflavor.za.forms import ZAPostCodeField


class ISPostalCodeField(CharField):
    """
    See https://en.wikipedia.org/wiki/List_of_postal_codes_in_Iceland for all valid postcodes.
    """

    def clean(self, value):
        codes, cities = zip(*IS_POSTALCODES)
        if not value in codes:
            raise ValidationError(
                ["Enter a postal code in the format NNN.", "See also https://en.wikipedia.org/wiki/List_of_postal_codes_in_Iceland."])
        return value


class GenericPostalCodeField(CharField):
    """
    Can add more validators if we'd like to later: https://en.wikipedia.org/wiki/List_of_postal_codes.
    This field validates the max_length of 20 and that string only contains alphanumeric characters, spaces, and dashes. 
    """

    def clean(self, value):
        self.max_length = 20
        self.min_length = 1
        pattern = r'^[a-zA-Z0-9][a-zA-Z0-9\- ]{0,10}[a-zA-Z0-9]'

        if len(value) > self.max_length or len(value) < self.min_length:
            raise ValidationError(
                f"Value must be between {self.min_length} and {self.max_length} characters.")
        if not re.match(pattern, value):
            raise ValidationError(
                f"Value must contain only alphanumeric characters, spaces, and dashes.")

        return value


postal_code_field_map = {
    "ar": ARPostalCodeField,
    "at": ATZipCodeField,
    "au": AUPostCodeField,
    "be": BEPostalCodeField,
    "br": BRZipCodeField,
    "ca": CAPostalCodeField,
    "ch": CHZipCodeField,
    "cn": CNPostCodeField,
    "cu": CUPostalCodeField,
    "cz": CZPostalCodeField,
    "de": DEZipCodeField,
    "dk": DKPostalCodeField,
    "ee": EEZipCodeField,
    "es": ESPostalCodeField,
    "fi": FIZipCodeField,
    "fr": FRZipCodeField,
    "gb": GBPostcodeField,
    "gr": GRPostalCodeField,
    "hr": HRPostalCodeField,
    "id": IDPostCodeField,
    "il": ILPostalCodeField,
    "in": INZipCodeField,
    "ir": IRPostalCodeField,
    "is": ISPostalCodeField,
    "it": ITZipCodeField,
    "jp": JPPostalCodeField,
    "lt": LTPostalCodeField,
    "lv": LVPostalCodeField,
    "ma": MAPostalCodeField,
    "mt": MTPostalCodeField,
    "mx": MXZipCodeField,
    "nl": NLZipCodeField,
    "no": NOZipCodeField,
    "nz": NZPostCodeField,
    "pk": PKPostCodeField,
    "pl": PLPostalCodeField,
    "pt": PTZipCodeField,
    "ro": ROPostalCodeField,
    "ru": RUPostalCodeField,
    "se": SEPostalCodeField,
    "sg": SGPostCodeField,
    "si": SIPostalCodeField,
    "sk": SKPostalCodeField,
    "tr": TRPostalCodeField,
    "ua": UAPostalCodeField,
    "us": USZipCodeField,
    "za": ZAPostCodeField,
}


def postal_code_validator(countryalpha2, postal_code):
    if countryalpha2.lower() in postal_code_field_map:
        postal_code_field_map[countryalpha2.lower()]().clean(postal_code)
    else:
        GenericPostalCodeField().clean(postal_code)
    return postal_code
