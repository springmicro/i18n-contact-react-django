const pkg = require("./addressData.js");
const { allCountries, allProvinces, regions, postalCodeLast, postalCodeNone } =
  pkg;
const fs = require("fs");
const filename = process.argv.slice(2)[0] + "contactData.json";

const objects = `{
  "countries": ${JSON.stringify(allCountries)},
  "provinces": ${JSON.stringify(allProvinces)},
  "regions": ${JSON.stringify(regions)}, 
  "postalCodeLast": ${JSON.stringify(postalCodeLast)}, 
  "postalCodeNone": ${JSON.stringify(postalCodeNone)}
}`;

fs.writeFile(filename, objects, function (err) {
  if (err) {
    return console.log(err);
  }
  console.log(filename + " was saved.");
});
