"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getProvinceValue = exports.getProvinceName = exports.postalCodeNone = exports.postalCodeLast = exports.regions = exports.allProvinces = exports.allCountries = void 0;
var provinces_1 = __importDefault(require("provinces"));
var countries = __importStar(require("i18n-iso-countries"));
var locale = __importStar(require("i18n-iso-countries/langs/en.json"));
countries.registerLocale(locale);
var allCountries = countries.getNames("en", { select: "official" });
exports.allCountries = allCountries;
// https://www.npmjs.com/package/provinces
// https://youbianku.com/files/upu/<countryAlpha3>.pdf
// Based on my own research (buckldav)
var regions = {
    oblast: ["RU"],
    prefecture: ["JP"],
    province: ["CA", "CN", "ES", "IN", "IT", "NG", "PH", "RU", "TR"],
    region: ["CL", "GB"],
    state: ["AU", "BR", "MX", "US", "VE"],
    territory: ["AU", "CA"],
};
exports.regions = regions;
function getProvinceName(province) {
    return ((province.region ? province.region : province.name) +
        (province.english && !province.region ? " (" + province.english + ")" : ""));
}
exports.getProvinceName = getProvinceName;
function getProvinceValue(province) {
    return province.region
        ? province.region
        : province.short
            ? province.short
            : province.name;
}
exports.getProvinceValue = getProvinceValue;
var allProvinces = provinces_1.default
    .filter(function (province) {
    // is a region we keep track of
    return Object.values(regions)
        .reduce(function (a, b) { return a.concat(b); }, [])
        .includes(province.country);
})
    .map(function (province) { return ({
    // include value prop in the object
    country: province.country,
    name: getProvinceName(province),
    value: getProvinceValue(province),
}); })
    .filter(function (v, i, a) {
    return a.findIndex(function (t) {
        return t.country === v.country && t.name === v.name && t.value === v.value;
    }) === i;
});
exports.allProvinces = allProvinces;
// http://www.bitboost.com/ref/international-address-formats.html#Formats
// https://youbianku.com/files/upu/<countryAlpha3>.pdf
// Based on my own research (buckldav)
var postalCodeLast = [
    "AU",
    "BR",
    "CA",
    "CR",
    "GB",
    "IN",
    "ID",
    "KH",
    "NZ",
    "PK",
    "US",
];
exports.postalCodeLast = postalCodeLast;
// https://en.wikipedia.org/wiki/List_of_postal_codes
var postalCodeNone = [
    "AO",
    "AG",
    "AW",
    "BS",
    "BZ",
    "BJ",
    "BM",
    "BO",
    "BW",
    "BF",
    "BI",
    "CM",
    "CF",
    "TD",
    "KM",
    "CG",
    "CK",
    "CI",
    "CW",
    "DJ",
    "DM",
    "TL",
    "GQ",
    "ER",
    "FJ",
    "TF",
    "GA",
    "GM",
    "GD",
    "GY",
    "HM",
    "HK",
    "KI",
    "KP",
    "LY",
    "MO",
    "MW",
    "ML",
    "MR",
    "NA",
    "NR",
    "NL",
    "NU",
    "QA",
    "RW",
    "KN",
    "ST",
    "SC",
    "SL",
    "SX",
    "SB",
    "SS",
    "SR",
    "SY",
    "TG",
    "TK",
    "TO",
    "TV",
    "UG",
    "AE",
    "VU",
    "YE",
    "ZW",
];
exports.postalCodeNone = postalCodeNone;
