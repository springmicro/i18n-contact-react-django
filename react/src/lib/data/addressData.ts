import { Alpha2Code } from "i18n-iso-countries";

import { Province } from "../@types/provinces";
import provinces from "provinces";
import * as countries from "i18n-iso-countries";
import * as locale from "i18n-iso-countries/langs/en.json";
countries.registerLocale(locale);
const allCountries = countries.getNames("en", { select: "official" });

type Region =
  | "oblast"
  | "prefecture"
  | "province"
  | "region"
  | "state"
  | "territory";

// https://www.npmjs.com/package/provinces
// https://youbianku.com/files/upu/<countryAlpha3>.pdf
// Based on my own research (buckldav)
const regions: Record<Region, Alpha2Code[]> = {
  oblast: ["RU"],
  prefecture: ["JP"],
  province: ["CA", "CN", "ES", "IN", "IT", "NG", "PH", "RU", "TR"],
  region: ["CL", "GB"],
  state: ["AU", "BR", "MX", "US", "VE"],
  territory: ["AU", "CA"],
};

function getProvinceName(province: Province) {
  return (
    (province.region ? province.region : province.name) +
    (province.english && !province.region ? " (" + province.english + ")" : "")
  );
}

function getProvinceValue(province: Province) {
  return province.region
    ? province.region
    : province.short
    ? province.short
    : province.name;
}

const allProvinces = provinces
  .filter((province: Province) =>
    // is a region we keep track of
    Object.values(regions)
      .reduce((a, b) => a.concat(b), [])
      .includes(province.country)
  )
  .map((province: Province) => ({
    // include value prop in the object
    country: province.country,
    name: getProvinceName(province),
    value: getProvinceValue(province),
  }))
  .filter(
    (v, i, a) =>
      a.findIndex(
        (t) =>
          t.country === v.country && t.name === v.name && t.value === v.value
      ) === i
  );

// http://www.bitboost.com/ref/international-address-formats.html#Formats
// https://youbianku.com/files/upu/<countryAlpha3>.pdf
// Based on my own research (buckldav)
const postalCodeLast: Alpha2Code[] = [
  "AU",
  "BR",
  "CA",
  "CR",
  "GB",
  "IN",
  "ID",
  "KH",
  "NZ",
  "PK",
  "US",
];

// https://en.wikipedia.org/wiki/List_of_postal_codes
const postalCodeNone: Alpha2Code[] = [
  "AO",
  "AG",
  "AW",
  "BS",
  "BZ",
  "BJ",
  "BM",
  "BO",
  "BW",
  "BF",
  "BI",
  "CM",
  "CF",
  "TD",
  "KM",
  "CG",
  "CK",
  "CI",
  "CW",
  "DJ",
  "DM",
  "TL",
  "GQ",
  "ER",
  "FJ",
  "TF",
  "GA",
  "GM",
  "GD",
  "GY",
  "HM",
  "HK",
  "KI",
  "KP",
  "LY",
  "MO",
  "MW",
  "ML",
  "MR",
  "NA",
  "NR",
  "NL",
  "NU",
  "QA",
  "RW",
  "KN",
  "ST",
  "SC",
  "SL",
  "SX",
  "SB",
  "SS",
  "SR",
  "SY",
  "TG",
  "TK",
  "TO",
  "TV",
  "UG",
  "AE",
  "VU",
  "YE",
  "ZW",
];

export {
  allCountries,
  allProvinces,
  regions,
  postalCodeLast,
  postalCodeNone,
  getProvinceName,
  getProvinceValue,
};
