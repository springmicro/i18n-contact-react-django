declare module "provinces" {}

export type Province = {
  country: Alpha2Code;
  short?: string;
  name: string;
  english?: string;
  region?: string;
  alt?: Array<string>;
};
