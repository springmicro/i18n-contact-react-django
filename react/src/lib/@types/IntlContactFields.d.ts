declare module "IntlContactFields";

export interface IntlContactFieldsProps {
  country2code: Alpha2Code;
  css?: {
    formControl?: {
      // (textarea, input)
      classes?: string[];
      invalidClasses?: string[];
      validClasses?: string[];
      invalidFeedbackClasses?: string[];
      validFeedbackClasses?: string[];
    };
    formFieldClasses?: string[]; // (div:parentOf(label, input))
    formLabelClasses?: string[]; // (label)
    formSelectClasses?: string[]; // (select)
    formSubmitClasses?: string[]; // button
    requiredAsteriskClasses?: string[]; // (already implemented)
  };
  errors?: any;
  setErrors?: Dispatch<any>;
}
