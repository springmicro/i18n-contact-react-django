import React, {
  useEffect,
  useState,
  Dispatch,
  SetStateAction,
  FormEvent,
} from "react";
import { Alpha2Code } from "i18n-iso-countries";
import provinces from "provinces";
import { Province } from "../@types/provinces";
import { PostalAddress } from "schema-dts";
import styles from "../styles/IntlContactFields.module.css";
import { IntlContactFieldsProps } from "../@types/IntlContactFields";
import { Input, Label } from "./Generic";
import {
  regions,
  postalCodeLast,
  postalCodeNone,
  allProvinces,
} from "../data/addressData";
import { ReactElement } from "react";

const countries = require("i18n-iso-countries");
countries.registerLocale(require("i18n-iso-countries/langs/en.json"));
const allCountries = countries.getNames("en", { select: "official" });

// from https://en.wikipedia.org/wiki/List_of_administrative_divisions_by_country
// from https://ux.stackexchange.com/questions/64665/address-form-field-for-region

type PostalCode = "PIN Code" | "Postal Code" | "Zip Code";

interface AddressFieldsProps extends IntlContactFieldsProps {
  setCountry2Code: Dispatch<SetStateAction<Alpha2Code | undefined>>;
  organizationFields?: ReactElement<any, any>;
  errors: any;
}

const getRegionLabel = (country2code: Alpha2Code): string => {
  let regionNames = (Object.keys(regions) as Array<keyof typeof regions>)
    .filter((key) => regions[key].includes(country2code))
    .map((w) => w[0].toUpperCase() + w.substr(1).toLowerCase());
  return regionNames.length === 0 ? "" : regionNames.join("/");
};

const getPostalCodeLabel = (country2code: Alpha2Code): PostalCode => {
  if (country2code === "US") {
    return "Zip Code";
  } else if (country2code === "IN") {
    return "PIN Code";
  } else {
    return "Postal Code";
  }
};

const getPostalCodeDefault = (country2code: Alpha2Code): string => {
  if (postalCodeNone.includes(country2code)) {
    return "00000";
  } else {
    return "";
  }
};

const getAddressLabels = (country2code: Alpha2Code): PostalAddress => ({
  "@type": "PostalAddress",
  streetAddress: "Street Address",
  postalCode: getPostalCodeLabel(country2code),
  addressLocality: "City",
  addressRegion: getRegionLabel(country2code),
});

const AddressFields: React.FC<AddressFieldsProps> = (props) => {
  const [fields, setFields] = useState<any>();
  const [addressRegionFieldOptional, setAddressRegionFieldOptional] =
    useState<boolean>(false);

  const validatePostalCode = async (event: FormEvent<HTMLInputElement>) => {
    const formData = new FormData();
    formData.append("addressCountry", props.country2code);
    formData.append("postalCode", (event.target as HTMLInputElement).value);

    const response = await fetch(
      "http://127.0.0.1:8000/api/address/validate_postal_code/",
      {
        method: "POST",
        body: formData,
      }
    );
    const json = await response.json();
    if (!response.ok) {
      // console.log(json);
      props.setErrors({ ...props.errors, postalCode: json });
    } else {
      if (props.errors) {
        const { postalCode, ...errors } = props.errors;
        props.setErrors(errors);
      }
    }
  };

  useEffect(() => {
    const fields = getAddressLabels(props.country2code);
    const fieldKeys = (
      Object.keys(fields) as Array<keyof typeof fields>
    ).filter((key) => key !== "@type" && fields[key]);
    if (postalCodeLast.includes(props.country2code)) {
      // Put the postalCode at the end of the address group
      fieldKeys.push(fieldKeys.splice(fieldKeys.indexOf("postalCode"), 1)[0]);
    }
    setFields(
      fieldKeys.map((name) => (
        <div
          key={name === "postalCode" ? props.country2code : name}
          className={
            props.css?.formFieldClasses
              ? `${styles.intlContact} ${props.css?.formFieldClasses.join(" ")}`
              : styles.intlContact
          }
        >
          <Label
            className={props.css?.formLabelClasses?.join(" ") as string}
            htmlFor={name}
            required={!(name === "addressRegion" && addressRegionFieldOptional)}
            requiredAsteriskClassName={props.css?.requiredAsteriskClasses?.join(
              " "
            )}
            extra={
              name === "postalCode" &&
              getPostalCodeDefault(props.country2code) ? (
                <>
                  Our records indicate that your country does not have a postal
                  code system, so "00000" will be used by default.
                </>
              ) : (
                <></>
              )
            }
          >
            {fields[name]}
          </Label>
          {name === "addressRegion" ? (
            addressRegionFieldOptional ? (
              <Input
                errors={props.errors ? props.errors[name] : undefined}
                name={name}
                id={name}
                required={false}
                className={props.css?.formControl?.classes?.join(" ")}
              />
            ) : (
              <select
                name={name}
                id={name}
                required={true}
                className={props.css?.formSelectClasses?.join(" ")}
              >
                {allProvinces
                  .filter((val) => val.country === props.country2code)
                  .map((province, i) => (
                    <option key={i} value={province.value}>
                      {province.name}
                    </option>
                  ))}
              </select>
            )
          ) : name === "streetAddress" ? (
            <textarea
              name={name}
              id={name}
              required={true}
              rows={2}
              className={props.css?.formControl?.classes?.join(" ")}
            />
          ) : (
            <Input
              errors={props.errors ? props.errors[name] : undefined}
              name={name}
              id={name}
              required={true}
              defaultValue={
                name === "postalCode"
                  ? getPostalCodeDefault(props.country2code)
                  : ""
              }
              onInput={name === "postalCode" ? validatePostalCode : undefined}
              css={props.css?.formControl}
            />
          )}
        </div>
      ))
    );
  }, [props.country2code, props.errors]);

  return (
    <>
      <div
        className={
          props.css?.formFieldClasses
            ? `${styles.intlContact} ${props.css?.formFieldClasses.join(" ")}`
            : styles.intlContact
        }
      >
        <Label
          className={props.css?.formLabelClasses?.join(" ")}
          htmlFor="addressCountry"
          required={true}
          requiredAsteriskClassName={props.css?.requiredAsteriskClasses?.join(
            " "
          )}
        >
          Country
        </Label>
        <select
          name="addressCountry"
          onChange={(e) => {
            const country2code = e.target.value as Alpha2Code;
            props.setCountry2Code(country2code);
            setAddressRegionFieldOptional(
              provinces.filter(
                (province: Province) => province.country === country2code
              ).length === 0
            );
          }}
          defaultValue="US"
          required={true}
          className={props.css?.formSelectClasses?.join(" ")}
        >
          {Object.keys(allCountries).map((key) => (
            <option key={key} value={key} selected={key === props.country2code}>
              {allCountries[key]}
            </option>
          ))}
        </select>
      </div>
      {props.country2code ? props.organizationFields : null}
      {props.country2code ? fields : null}
    </>
  );
};

export default AddressFields;
export { getAddressLabels, getRegionLabel };
