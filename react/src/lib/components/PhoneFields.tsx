import React, { useState, useEffect, Dispatch } from "react";
import { Alpha2Code } from "i18n-iso-countries";
import IntlTelInput from "react-intl-tel-input";
import { CountryData } from "react-intl-tel-input";
import "react-intl-tel-input/dist/main.css";
import styles from "../styles/IntlContactFields.module.css";
import { IntlContactFieldsProps } from "../@types/IntlContactFields";
import { Label } from "./Generic";

type PhoneFieldsProps = {
  setPhone?: Dispatch<any>;
  unique: boolean;
};

const PhoneFields: React.FC<PhoneFieldsProps & IntlContactFieldsProps> = (
  props
) => {
  const [pattern, setPattern] = useState<string>("");

  const errorMessage = () => {
    return [
      "Please match the requested format: " + pattern.replace(/\d/g, "#"),
    ];
  };

  const validatePhoneUnique = async (telephone: string) => {
    const formData = new FormData();
    formData.append("telephone", telephone);

    const response = await fetch(
      "http://127.0.0.1:8000/api/phone/validate_phone_unique/",
      {
        method: "POST",
        body: formData,
      }
    );
    const json = await response.json();
    if (!response.ok) {
      // console.log(json);
      props.setErrors({ ...props.errors, telephone: json });
    } else {
      if (props.errors) {
        const { telephone, ...errors } = props.errors;
        props.setErrors(errors);
      }
    }
  };

  const telephoneOnChange = (
    isValid: boolean,
    value: string,
    seletedCountryData: CountryData,
    fullNumber: string,
    extension: string
  ) => {
    // console.log(pattern);
    if (typeof props.setErrors === "function") {
      if (isValid || fullNumber.length === 0) {
        if (props.errors) {
          const { telephone, ...errors } = props.errors;
          props.setErrors(errors);
        }
        if (isValid && props.unique) {
          validatePhoneUnique(fullNumber);
        }
        // console.log(fullNumber);
      } else {
        // console.log(errorMessage());
        props.setErrors({ ...props.errors, telephone: errorMessage() });
      }
    }
    if (typeof props.setPhone === "function") {
      props.setPhone(fullNumber);
    }
  };

  useEffect(() => {
    // Clear telephone errors (if any)
    telephoneOnChange(false, "", { iso2: props.country2code }, "", "");
  }, [props.country2code]);

  return (
    <div
      className={
        props.css?.formFieldClasses
          ? `${styles.intlContact} ${props.css?.formFieldClasses.join(" ")}`
          : styles.intlContact
      }
    >
      <Label
        htmlFor="telephone"
        required={true}
        requiredAsteriskClassName={props.css?.requiredAsteriskClasses?.join(
          " "
        )}
        extra={
          <>
            To include an extension, follow this format:
            <br />
            <span>{pattern} ext. 123</span>
          </>
        }
      >
        Phone Number
      </Label>
      <IntlTelInput
        key={props.country2code}
        customPlaceholder={(
          selectedCountryPlaceholder,
          selectedCountryData
        ) => {
          setPattern(selectedCountryPlaceholder);
          return selectedCountryPlaceholder;
        }}
        inputClassName={
          props.errors && props.errors["telephone"]
            ? `${props.css?.formControl?.classes?.join(
                " "
              )} ${props.css?.formControl?.invalidClasses?.join(" ")}`
            : props.css?.formControl?.classes?.join(" ")
        }
        fieldId="telephone"
        fieldName="telephone"
        defaultCountry={props.country2code.toLowerCase()}
        onPhoneNumberChange={telephoneOnChange}
      />
      {props.errors && props.errors["telephone"] ? (
        <>
          {/* This hidden input is included because the invalid feedback has to be a sibling of a input element (IntlTelInput is wrapped in a div). */}
          <input
            type="hidden"
            className={props.css?.formControl?.invalidClasses?.join(" ")}
          />
          {props.errors["telephone"].map((e: string) => (
            <p
              key={e}
              className={props.css?.formControl?.invalidFeedbackClasses?.join(
                " "
              )}
            >
              {e}
            </p>
          ))}
        </>
      ) : null}
    </div>
  );
};

export default PhoneFields;
