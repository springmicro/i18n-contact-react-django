import React, { FormHTMLAttributes, useEffect, useState } from "react";
import AddressFields from "./AddressFields";
import PhoneFields from "./PhoneFields";
import { Alpha2Code } from "i18n-iso-countries";
import styles from "../styles/IntlContactFields.module.css";
import OrganizationFields from "./OrganizationFields";
import { Data } from "../../App";
import { Dispatch, SetStateAction } from "react";

const FieldStyles = {
  formControl: {
    classes: ["form-control"],
    invalidClasses: ["is-invalid"],
    invalidFeedbackClasses: ["invalid-feedback"],
    validClasses: ["is-valid"],
    validFeedbackClasses: ["valid-feedback"],
  },
  formSelectClasses: ["form-select"],
  formLabelClasses: ["form-label"],
  formFieldClasses: ["mb-2"],
  formSubmitClasses: ["btn btn-primary"],
  requiredAsteriskClasses: ["required"],
};

type GeolocationAddressComponent = {
  types: Array<string>;
  short_name: string;
  long_name: string;
};

interface ContactFormProps {
  data?: Data;
  errors?: any;
  setErrors?: Dispatch<any>;
  setPhone?: Dispatch<any>;
}

const getCountry = (geolocationResults: Array<any>) => {
  let country = "";
  for (const result of geolocationResults) {
    country = result?.address_components.find(
      (val: GeolocationAddressComponent) => val.types.includes("country")
    ).short_name;
    if (country) {
      return country;
    }
  }
  return country;
};

const ContactForm: React.FC<
  ContactFormProps & FormHTMLAttributes<HTMLFormElement>
> = (props) => {
  const { className, children, setErrors, setPhone, ...rest } = props;
  const [country, setCountryState] = useState<Alpha2Code>();

  const setCountry: Dispatch<SetStateAction<Alpha2Code | undefined>> = (
    country
  ) => {
    // Clear all input fields on country change
    (document.getElementById("contactForm") as HTMLFormElement).reset();
    setCountryState(country);
  };

  useEffect(() => {
    async function getLocation() {
      if (
        "REACT_APP_GEOCODING_API_KEY" in process.env &&
        navigator.geolocation
      ) {
        navigator.geolocation.getCurrentPosition(async (pos) => {
          // console.log(pos.coords.latitude, pos.coords.longitude);
          const response = await fetch(
            `https://maps.googleapis.com/maps/api/geocode/json?latlng=${pos.coords.latitude},${pos.coords.longitude}&key=${process.env.REACT_APP_GEOCODING_API_KEY}`
          );
          const json = await response.json();
          if (response.ok) {
            setCountry(getCountry(json?.results) as Alpha2Code);
          } else {
            console.warn("Geolocation request unsuccessful");
          }
        });
      }
    }

    getLocation();
  }, []);

  return (
    <form
      className={`${styles.intlContact} ${className}`}
      {...rest}
      id="contactForm"
    >
      <div role="group" aria-labelledby="address_head">
        <h4 id="address_head">Address</h4>
        {props.data?.address ? (
          <div>
            {props.data.address.split("\n").map((address_line) => (
              <React.Fragment key={address_line}>
                {address_line}
                <br />
              </React.Fragment>
            ))}
          </div>
        ) : (
          <AddressFields
            country2code={country}
            setCountry2Code={setCountry}
            css={FieldStyles}
            errors={props.errors}
            setErrors={setErrors}
            organizationFields={<OrganizationFields css={FieldStyles} />}
          />
        )}
      </div>
      {country ? (
        <div role="group" aria-labelledby="phone_head">
          <h4 id="phone_head">Phone Number</h4>
          {props.data?.phone ? (
            <div>{props.data.phone}</div>
          ) : (
            <PhoneFields
              country2code={country}
              css={FieldStyles}
              errors={props.errors}
              setErrors={setErrors}
              setPhone={setPhone}
              unique={true}
            />
          )}
        </div>
      ) : null}
      {country ? (
        <input
          disabled={!!props.errors && !!Object.keys(props.errors).length}
          type="submit"
          value="Submit"
          className={FieldStyles.formSubmitClasses.join(" ")}
        />
      ) : null}
    </form>
  );
};

export default ContactForm;
