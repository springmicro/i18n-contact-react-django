import React, { ReactFragment, FormEventHandler } from "react";
import { ReactNode } from "react";

interface LabelProps extends React.LabelHTMLAttributes<HTMLLabelElement> {
  htmlFor: string;
  required: boolean;
  requiredAsteriskClassName?: string;
  extra?: ReactFragment;
}

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  required: boolean;
  errors?: string[];
  css?: {
    classes?: string[];
    invalidClasses?: string[];
    invalidFeedbackClasses?: string[];
    validClasses?: string[];
    validFeedbackClasses?: string[];
  };
}

const Label: React.FC<LabelProps> = (props) => {
  const {
    children,
    extra,
    required,
    requiredAsteriskClassName,
    ...labelProps
  } = props;
  return (
    <label {...labelProps}>
      {children}{" "}
      {required ? (
        <abbr title="required" className={requiredAsteriskClassName}>
          *
        </abbr>
      ) : null}
      {extra ? (
        <>
          <br />
          <small>{extra}</small>
        </>
      ) : null}
    </label>
  );
};

const Input: React.FC<InputProps> = (props) => {
  const { className, errors, value, ...rest } = props;
  const inputClasses: string[] = [];
  if (props.css?.classes) inputClasses.push(props.css.classes.join(" "));
  if (errors) {
    if (props.css?.invalidClasses)
      inputClasses.push(props.css.invalidClasses.join(" "));
  }
  const feedbackClasses: string[] = [];
  if (errors) {
    if (props.css?.invalidFeedbackClasses)
      feedbackClasses.push(props.css.invalidFeedbackClasses.join(" "));
  }

  return (
    <>
      <input
        className={inputClasses.join(" ")}
        placeholder={props.required ? props.placeholder : "(optional)"}
        {...rest}
      />
      {errors
        ? errors.map((e) => <p className={feedbackClasses.join(" ")}>{e}</p>)
        : null}
    </>
  );
};

export { Label, Input };
