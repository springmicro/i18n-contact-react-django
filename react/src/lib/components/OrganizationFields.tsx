import React from "react";
import { IntlContactFieldsProps } from "../@types/IntlContactFields";
import styles from "../styles/IntlContactFields.module.css";
import { Input, Label } from "./Generic";

type OrganizationFieldsProps = Omit<IntlContactFieldsProps, "country2code">;

const OrganizationFields: React.FC<OrganizationFieldsProps> = (props) => {
  return (
    <div
      className={
        props.css?.formFieldClasses
          ? `${styles.intlContact} ${props.css?.formFieldClasses?.join(" ")}`
          : styles.intlContact
      }
    >
      <Label
        required={false}
        htmlFor="organization"
        className={props.css?.formLabelClasses?.join(" ")}
        extra={
          <>Only include company name if you want it included on the address.</>
        }
      >
        Company Name
      </Label>
      <Input
        name="organization"
        id="organization"
        required={false}
        css={props.css?.formControl}
      />
    </div>
  );
};

export default OrganizationFields;
