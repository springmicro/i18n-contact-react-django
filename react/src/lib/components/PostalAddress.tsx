import React, { PropsWithChildren } from "react";
import { PostalAddress as PostalAddressType } from "schema-dts";

const PostalAddress = (props: PropsWithChildren<PostalAddressType>) => {
  return (
    <div
      itemProp="address"
      itemScope
      itemType="https://schema.org/PostalAddress"
    >
      <span itemProp="streetAddress">{props.streetAddress as string}</span>
      <br />
      <span itemProp="addressLocality">
        {props.addressLocality as string}
      </span>,{" "}
      <span itemProp="addressRegion">{props.addressRegion as string}</span>{" "}
      <span itemProp="postalCode">{props.postalCode as string}</span>
    </div>
  );
};

export default PostalAddress;
