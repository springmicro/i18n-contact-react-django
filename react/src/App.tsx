import React, { FormEvent, useState } from "react";
import { ContactForm } from "./lib";

export type Data = {
  address?: string;
  phone?: string;
};

function App() {
  const [errors, setErrors] = useState<any>();
  const [data, setData] = useState<Data>({});
  const [phone, setPhone] = useState<string>("");

  const handleSubmit = async (event: FormEvent) => {
    event.preventDefault();
    const formData = new FormData(event.target as HTMLFormElement);
    if (!Object.keys(data).includes("phone")) {
      formData.set("telephone", phone);
    }

    // for (const [key, value] of formData.entries()) {
    //   console.log(key, value);
    // }

    const response = await fetch("http://127.0.0.1:8000/create-contact/", {
      method: "POST",
      body: formData,
      headers: {
        Authorization: `Token ${process.env.REACT_APP_USER_TOKEN_DEV}`,
      },
    });
    const json = await response.json();
    if (response.ok) {
      console.log(json);
      setData({ ...data, ...json.data });
      setErrors(json.errors);
    } else {
      setErrors(json);
      console.log(json);
    }
  };

  return (
    <>
      <ContactForm
        data={data}
        errors={errors}
        setErrors={setErrors}
        setPhone={setPhone}
        onSubmit={handleSubmit}
      />
    </>
  );
}

export default App;
