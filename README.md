# intl-contact-react-django

By David Buckley, [dbuckley@springmicro.com](dbuckley@springmicro.com)

> **WARNING: Do not run the React App and Django Project as is in production. They are meant for demonstration purposes only.**

A React form for international addresses and phone numbers of all shapes and sizes. Based on https://schema.org/PostalAddress and https://schema.org/Person.
Includes Django models, serializers, views, and viewsets for Addresses and Phones.

## How it works:

![diagram](/docs/flow.png)

In production, the addresses are stored on the CRM, so an API Key is needed to access the endpoints. This api behavior is emulated in this sandbox.

## Set Up

After cloning the repository...

### Set Up Django Project

```bash
cd django
python -m venv env
source env/bin/activate
pip install -r requirements.txt
cp .env.example .env
```

```bash
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```

Go to the [admin page for Tokens](http://127.0.0.1:8000/admin/authtoken/tokenproxy/add/) and create a token for your superuser.
Store the token in .env under `USER_TOKEN_DEV`.

```
USER_TOKEN_DEV="b0c6516773c4f9a93a6fd1d26397baf50e2e16c5"
```

Go to the [admin page for API Keys](http://127.0.0.1:8000/admin/rest_framework_api_key/apikey/add/) and create an API Key.
Store the token in .env under `REST_API_KEY`.
This API Key will only be used server-side.

#### Django Tests

```bash
python manage.py test
```

### Set Up React App

```bash
cd react
npm install
cp .env.example .env
```

Get the same user token from `/django/.env` and store in `REACT_APP_USER_TOKEN_DEV`.

```bash
# Gets the same country data used to validate the frontend and
# puts it in the json/ folder which can be accessed by the backend.
npm run json
```

## Features

### Google Geocoding API

This app supports setting the country based on the user's location. To enable this, get an API key from Google's [Geocoding API](https://developers.google.com/maps/documentation/geocoding/get-api-key). Store the key in `.env` under `REACT_APP_GEOCODING_API_KEY`.

### Address Fields

Address Fields change based on the country. However, the input names and ids will always match with [Schema.org's definition for PostalAddress](https://schema.org/PostalAddress).

PO Box or Street Address, but not both. PO Box not P.O. Box
https://bizfluent.com/how-4896711-address-envelope-po-box.html

```
addressCountry	    Country or Text	The country. For example, USA. You can also provide the two-letter ISO 3166-1 alpha-2 country code.
addressLocality	    Text	        The locality in which the street address is, and which is in the region. For example, Mountain View.
addressRegion	    Text	        The region in which the locality is, and which is in the country. For example, California or another appropriate first-level Administrative division
postOfficeBoxNumber Text	        The post office box number for PO box addresses.
postalCode	    Text	        The postal code. For example, 94043.
streetAddress	    Text	        The street address. For example, 1600 Amphitheatre Pkwy.
```

#### Address Format Definitions

In [/react/src/lib/components/addressData.ts](/react/src/lib/components/addressData.ts) are the following objects:

- `regions`: Countries with `addressRegions` (that are required to include in an address, see definition).
- `postalCodeLast`: Countries that put the `postalCode` after the `addressLocality`.
- `postalCodeNone`: Countries without `postalCodes` (from [Wikipedia](https://en.wikipedia.org/wiki/List_of_postal_codes), 7/10/2021).

### Phone Fields

Phone Fields are required to be unique (as defined in [/react/src/lib/components/ContactForm.tsx](/react/src/lib/components/ContactForm.tsx)).

## More Information

### Form Research and Rationale

- Why use a textarea for street address: https://baymard.com/blog/address-line-2
- Grouping things together: https://www.w3.org/WAI/tutorials/forms/grouping/
- Form instructions: https://www.w3.org/WAI/tutorials/forms/instructions/
